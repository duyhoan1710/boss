let id_list = ['password','email','fullname','student_id','major','date_of_birth','class_name'];
let setInputDisabled = (arr) =>{
    for (let i =0; i < arr.length; i++){
        $(`#${arr[i]}`).attr("disabled", true);
    }
}
let setInputAbled = (arr) =>{
    for (let i =0; i < arr.length; i++){
        $(`#${arr[i]}`).attr("disabled", false);
    }
}
$(document).ready(()=>{
    setInputDisabled(id_list);
    $('#btn_edit').click(()=>{

        if($('#btn_edit').val()==1) {
            $('#form-info').submit();
        }else{
            $('#btn_edit').val(1);
            setInputAbled(id_list);
            $('#btn_edit').text("Save").removeClass('btn-secondary').addClass('btn-primary');
        }
    });
});