<?php 
        require "connect.php";
        require "library.php";
        $db_database="database_boss_tuan";
        $db_connect=new PDO("mysql:host=localhost;dbname=database_boss_tuan",'root','');
        $username=isset($_POST['username']) ? $_POST['username'] : '';
        $password=isset($_POST['password']) ? $_POST['password'] : '';
        $error="";
        if($username==''||$password==''){
            die("vui lòng điền đủ thông tin !");
        }
        if(check_user($username)) die("Tài khoản không được chứa kí tự đặc biệt !");
        if(check_pass($password)) die("Mật khẩu phải có ít nhất 8 kí tự , trong đó phải có số , chữ in hoa !"); 
        
        if($error==''){
            $db_table="user";
            $sql="SELECT * FROM user WHERE username= :username AND password = :password";
            $db_record=$db_connect->prepare($sql);
            $db_record->bindParam(':username',$username);
            $db_record->bindParam(':password',$password);
            $db_record->execute();
            $setResuil=$db_record->fetch(PDO::FETCH_ASSOC);
            if($setResuil){
                $sql="SELECT lever FROM user WHERE username= :username AND password = :password";
                $db_record=$db_connect->prepare($sql);
                $db_record->bindParam(':username',$username);
                $db_record->bindParam(':password',$password);
                $db_record->execute();
                $setResuil=$db_record->fetch(PDO::FETCH_ASSOC);
                if($setResuil['lever']=='0'){
                    setcookie('user',$_POST['username'],time()+(86400*30), "/");
                    die('0');
                }
                else if($setResuil['lever']==1){
                    setcookie('admin',$_POST['username'],time()+(86400*30), "/");
                    die('1');
                }  
            }
            else{
                die("Username hoặc pass không hợp lệ !");
            } 
        }
        echo $error;
?>

