<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/javascript.js"></script>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="icon" href="./images/favicon.ico" type="image/x-icon" />
    <title>Student Manager</title>
</head>
<?php   
    $admin = isset($_COOKIE['admin']) ? $_COOKIE['admin'] : '';
    $user = isset($_COOKIE['user']) ? $_COOKIE['user'] : ''; 
    if($admin) { 
        header("Location:manager.php");     
    }
    else if($user){ 
        header("Location:user.html");
    }
?>   
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Student Manager</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li><a class="nav-link" href="login.html">login</a></li>

            </ul>
        </div>
    </nav>
    <div class="main-content">
        <div class="info-user">
            <form id="form-info" action="#" method="put">
            <div class="input-group mb-3">
            <button id="btn_edit" type="button" class="btn btn-secondary" value="0">Edit</button>
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Username</span>
                    </div>
                    <input id="username" type="text" class="form-control" placeholder="Username" aria-label="Username" >          
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Password</span>
                    </div>
                    <input id="password" type="text" class="form-control" placeholder="Password" aria-label="Password" >          
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Email</span>
                    </div>
                    <input id="email" type="text" class="form-control" placeholder="Email" aria-label="Email" >          
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Name</span>
                    </div>
                    <input id="fullname" type="text" class="form-control" placeholder="Fullname" aria-label="Fullname" >          
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Date of birth</span>
                    </div>
                    <input id="date_of_birth" type="text" class="form-control" placeholder="Date of birth" aria-label="Date of birth" >          
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Class</span>
                    </div>
                    <input id="class_name" type="text" class="form-control" placeholder="Class" aria-label="Class" >          
            </div>
            <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Major</span>
                    </div>
                    <input id="major" type="text" class="form-control" placeholder="Major" aria-label="Major" >          
            </div>
            </form>
        </div>
    </div>
</body>

</html>