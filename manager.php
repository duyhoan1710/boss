<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <title>Student Manager</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Student Manager</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="file_php/logout.php">Logout</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="edit.html">Edit User</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="main-content">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">username</th>
                    <th scope="col">password</th>
                    <th scope="col">Fullname</th>
                    <th scope="col">Class</th>
                    <th scope="col">Date Of Birth</th>
                    <th scope="col">Email</th>   
                    <th scope="col">Major</th> 
                    <th scope="col">Lever</th>   
                    <th scope="col">Action</th>
                    
                </tr>
            </thead>
            <tbody>
                        <?php 
                            require "file_php/connect.php";
                            $db_database="database_boss_tuan";
                            $db_connect=new PDO("mysql:host=localhost;dbname=database_boss_tuan",'root','');
                            $sql="SELECT * FROM user";
                            $db_record=$db_connect->prepare($sql);  
                            $db_record->execute();
                            $setResuil=$db_record->fetch(PDO::FETCH_ASSOC);
                         ?>                    
                     <?php while($row = $db_record->fetch()) { ?>
                      <tr>
                        <td><?php echo $row['username']; ?></td>
                        <td><?php echo $row['password']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['dateOfBirth']; ?></td>
                        <td><?php echo $row['class']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['major']; ?></td>
                        <td><?php echo $row['lever']; ?></td>
                        <td>
                            <form method="post" action="file_php/delete.php">
                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>"/>
                                <input onclick="return confirm('Bạn có chắc muốn xóa không?');" type="submit" name="delete" value="Delete"/>
                            </form>
                        </td>
                      </tr>
                      <?php } ?>
            </tbody>
        </table>
    </div>
</body>

</html>